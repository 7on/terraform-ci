resource default_vpc {
    cidr_block = ["10.0.0.0/16"]
    enable_dns_support = true 
    enable_dns_hostnames = true 
    enable_classiclink = false
    instance_tenancy = default    
    
    tags {
        Name = default-vpc
    }
}

resource default_subnet {
    vpc_id = "${default_vpc.id}"
    cidr_block = ["10.0.0.0/24"]
    map_public_ip_on_launch = true //it makes this a public subnet
    availability_zone = us-east-1a
    tags {
        Name = default-subnet-public-1
    }
}